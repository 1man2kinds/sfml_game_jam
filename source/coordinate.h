#ifndef COORDINATE_H
#define COORDINATE_H

// This file defines the tile coordinates in the game as well as the interactions with them

#include <SFML/System.hpp>
#include <queue>

// The size of the map in tiles
const int MAP_WIDTH = 20;
const int MAP_HEIGHT = 16;

// The size of each tile in pixels
const int TILE_SIZE = 32;

// A coordinate can be defined as a 2d vector
typedef sf::Vector2i Coordinate;

enum class Direction { EAST, WEST, NORTH, SOUTH };

// A cubie state can be defined as a coordinate and a direction
struct CubieState
{
	const Coordinate coordinate;
	const Direction direction;
	inline CubieState(const Coordinate& coord, const Direction dir) : coordinate(coord), direction(dir) { }
};

// Check if two cordinates are adjacent
bool areAdjacent(const Coordinate& first, const Coordinate& second);

// Given a coordinate and a direction, Return the return the coordinate offset by 1 in the given direction
Coordinate getCoordinate(const Coordinate& coord, const Direction dir);

// Given two coordinates that are adjacent 
// Return the direction needed to take from origin to reach the target
Direction findDirection(const Coordinate& origin, const Coordinate& target);

#endif // COORDINATE_H
