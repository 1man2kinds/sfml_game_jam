#include "player.h"
#include "resources.h"

Player::Player(const Coordinate& pos) : Cubie(pos)
{
	mDirection = DEFUALT_DIRECTION;
}

void Player::tick()
{
	move(mDirection);
}

void Player::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	sf::Sprite sprite;
	sprite.setTexture(Resources::getTexture("player"));
	sprite.setPosition(mPosition.x * TILE_SIZE + (sprite.getGlobalBounds().width / 2.0f), mPosition.y * TILE_SIZE + (sprite.getGlobalBounds().height / 2.0f));
	
	// center the sprite for the rotation
	sprite.setOrigin(sprite.getGlobalBounds().width / 2.0f, sprite.getGlobalBounds().height / 2.0f);
	float angle = 0.0f;

	switch(mDirection){
	case Direction::NORTH:
		angle = 0.0f;
		break;
	case Direction::EAST:
		angle = 90.0f;
		break;
	case Direction::SOUTH:
		angle = 180.0f;
		break;
	case Direction::WEST:
		angle = 270.0f;
		break;
	}
	sprite.setRotation(angle);
	target.draw(sprite);
}
