#ifndef CUBIE_MANAGER_H
#define CUBIE_MANAGER_H

#include <SFML/Graphics.hpp>
#include <vector>

#include "resources.h"
#include "coordinate.h"
#include "cubie.h"
#include "ghost.h"
#include "player.h"

class CubieManager
{
private:
	const Coordinate INITIAL_POSITION; // The intial position where the player starts
	std::vector<Ghost*> ghosts;
	
	std::vector<CubieState> history; // The history of every move since the beginning of the game
	Coordinate candy; // All the candy in the game

	Player* player;

	int score;
	int energy;

	// Loop back to the beginning of time
	void loop();
public:
	CubieManager(int tickAmount);
	~CubieManager();

	// Restart the game
	void restart();

	void tick();

	void draw(sf::RenderTarget& target);

	const sf::String getScoreString();
	const sf::String getEnergyString();
	inline int getEnergy() { return energy; }
	// Change the direction of the player
	void setPlayerDirection(const Direction direction);
};

#endif // CUBIE_MANAGER_H
