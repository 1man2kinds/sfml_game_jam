#ifndef RESOURCES_H
#define RESOURCES_H

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <map>

class Resources
{
private:
	static std::map<sf::String, sf::Texture> mTextures;
	static std::map<sf::String, sf::Sound> mSounds;
	static std::map<sf::String, sf::Font> mFonts;

	static sf::SoundBuffer* collision;
	static sf::SoundBuffer* loop;
public:
	// Load all the resources into the game
	static void load();

	// Unload all of the resources
	static void unload();

	// Return the texture with the given name
	static const sf::Texture& getTexture(const sf::String& name);
	
	// Return the sound with the given name
	static sf::Sound& getSound(const sf::String& name);

	// Return the default font
	static const sf::Font& getFont();

};

#endif // RESOURCES_H
