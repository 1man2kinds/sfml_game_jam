#include "cubie_manager.h"
#include "game_over.h"
#include <sstream>

CubieManager::CubieManager(int tickAmount) : INITIAL_POSITION(Coordinate(MAP_WIDTH / 2, MAP_HEIGHT / 2))
{
	player = NULL;
	srand(time(0));
}

CubieManager::~CubieManager()
{
	for(Ghost* ghost : ghosts){
		delete ghost;
	}
	ghosts.clear();

}

void CubieManager::restart()
{
	// Remove all inforamtion from previous game
	for(Ghost* ghost : ghosts)
		delete ghost;
	ghosts.clear();
	history.clear();
	score = 0;
	energy = 27;
	// Create a new candy
	candy.x = rand() % MAP_WIDTH;
	candy.y = rand() % MAP_HEIGHT;

	// (re)create the player
	if(player)
		delete player;
	player = new Player(INITIAL_POSITION);
}

void CubieManager::loop()
{	
	Resources::getSound("loop").play();
	score += 30 + ghosts.size() * 5;
	energy += 18;

	// Add a new ghost at the start point 
	Ghost* newGhost = new Ghost(INITIAL_POSITION);
	ghosts.push_back(newGhost);

}

void CubieManager::tick()
{
	if(energy <= 0)
		throw game_over();
	else
		energy--;
	score += ghosts.size() + 1;

	Coordinate newPos = getCoordinate(player->getPosition(), player->getDirection());
	// Make sure there is no collision
	for( Ghost* other : ghosts ){
		if(other->getPosition() == newPos)
			throw game_over(CubieState(other->getPosition(), other->getDirection()), CubieState(player->getPosition(), player->getDirection()));
	}
	player->tick();

	if(player->getPosition() == candy){
		candy.x = rand() % MAP_WIDTH;
		candy.y = rand() % MAP_HEIGHT;
		loop();
	}

	history.push_back(CubieState(player->getPosition(), player->getDirection()));

	for(Ghost* ghost : ghosts){
		ghost->tick();
		CubieState newState = history.at(ghost->getHistoryIndex());
		ghost->setDirection(newState.direction);
		// Make sure the ghosts don't collide with each other or with the player
		for( Ghost* other : ghosts ){
			if(other != ghost && other->getPosition() == newState.coordinate)
				throw game_over(CubieState(other->getPosition(), other->getDirection()), CubieState(ghost->getPosition(), ghost->getDirection()));
		}
		if(newState.coordinate == player->getPosition())
				throw game_over(CubieState(player->getPosition(), player->getDirection()), CubieState(ghost->getPosition(), ghost->getDirection()));
		ghost->setPosition(newState.coordinate);
	}
	
}

void CubieManager::draw(sf::RenderTarget& target)
{
	sf::RectangleShape shape(sf::Vector2f(TILE_SIZE, TILE_SIZE));

	// Draw the history
	for(CubieState state : history){
		shape.setFillColor(sf::Color(240, 240, 240));
		shape.setPosition(state.coordinate.x * TILE_SIZE, state.coordinate.y * TILE_SIZE);
		target.draw(shape, sf::RenderStates(sf::BlendMultiply));
	}

	// Draw the candy
	shape.setFillColor(sf::Color::Green);
	sf::Sprite sprite(Resources::getTexture("mushroom"));
	sprite.setPosition(candy.x * TILE_SIZE, candy.y * TILE_SIZE);
	target.draw(sprite);

	// Draw the player
	target.draw(*player);
	
	// Draw all the ghosts
	shape.setFillColor(sf::Color::Blue);
	
	for(Ghost* ghost : ghosts){
		target.draw(*ghost);
	}
}

const sf::String CubieManager::getScoreString()
{
	std::stringstream scoreStr;
	scoreStr << "Score: " << score;
	return sf::String(scoreStr.str());
}

const sf::String CubieManager::getEnergyString()
{
	std::stringstream energyStr;
	energyStr << "Energy: " << energy;
	return sf::String(energyStr.str());
}


void CubieManager::setPlayerDirection(const Direction direction)
{
	player->setDirection(direction);
}
