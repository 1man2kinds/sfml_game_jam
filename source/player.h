#ifndef PLAYER_H
#define PLAYER_H

#include "coordinate.h"
#include "cubie.h"
#include <vector>
#include <SFML/Graphics.hpp>

class Player : public Cubie
{
	static const Direction DEFUALT_DIRECTION = Direction::EAST;


public:
	Player(const Coordinate& pos);

	void tick();

	// Draw the player onto a render target
	void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};

#endif // PLAYER_H
